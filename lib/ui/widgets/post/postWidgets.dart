import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:image_your_reddit/data/model/post.dart';
import 'package:image_your_reddit/ui/screens/postViewScreen.dart';
import 'package:image_your_reddit/ui/widgets/post/imagePostWidget.dart';
import 'package:image_your_reddit/ui/widgets/post/textPostWidget.dart';

const POST_HEIGHT = 400.0;
const POST_TITLE_HEIGHT = 100.0;

/* so that post widgets don't take the full size of the screen, thus the user
understands he can swipe horizontally */
const POST_LIST_HORIZONTAL_OFFSET = 60;

class PostWidget extends StatelessWidget {
  final Post post;

  PostWidget(this.post);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          showPost(context, this.post);
        },
        child: post.thumbnail != null
            ? ImagePostWidget(post)
            : TextPostWidget(post));
  }
}

class PostContainer extends StatelessWidget {
  final Widget child;
  final Post post;

  PostContainer(this.post, {this.child});

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width - 60;
    final theme = Theme.of(context);

    return Container(
        width: width,
        margin: EdgeInsets.symmetric(horizontal: 8.0),
        decoration: BoxDecoration(color: theme.backgroundColor, boxShadow: [
          BoxShadow(
              blurRadius: 6, color: Colors.black26, offset: Offset(2.0, 4.0))
        ]),
        child:
            Stack(children: [child, UpVoteButtonWidget(post, theme: theme)]));
  }
}

class UpVoteButtonWidget extends StatelessWidget {
  final ThemeData theme;
  final Post post;

  const UpVoteButtonWidget(this.post, {Key key, this.theme}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
        bottom: 10,
        right: 10,
        child: ButtonTheme(
            child: FloatingActionButton(
          heroTag: "upVote${post.hashCode}",
          backgroundColor: theme.primaryColor,
          child: Icon(Icons.arrow_upward, color: theme.primaryColorLight),
          onPressed: () => showPost(context, post),
        )));
  }
}

class PostTitleWidget extends StatelessWidget {
  final String _text;

  PostTitleWidget(this._text);

  @override
  Widget build(BuildContext context) {
    double width =
        MediaQuery.of(context).size.width - POST_LIST_HORIZONTAL_OFFSET;

    final theme = Theme.of(context);

    return Container(
        height: POST_TITLE_HEIGHT,
        width: width,
        padding: EdgeInsets.symmetric(horizontal: 8.0),
        decoration: BoxDecoration(color: theme.backgroundColor, boxShadow: [
          BoxShadow(
              blurRadius: 36, color: Colors.black38, offset: Offset(0.0, 4.0))
        ]),
        child: Center(child: new PostTitleTextWidget(text: _text)));
  }
}

class PostTitleTextWidget extends StatelessWidget {
  final String _text;

  const PostTitleTextWidget({
    Key key,
    @required String text,
  })  : _text = text,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);

    return Container(
        padding: EdgeInsets.all(8.0),
        child: Text(this._text,
            maxLines: 4,
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
                color: theme.primaryColorLight)));
  }
}

class PostList extends StatelessWidget {
  final children;

  PostList(this.children);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: POST_HEIGHT,
        child: CarouselSlider(
            height: POST_HEIGHT,
            scrollDirection: Axis.horizontal,
            items: children));
  }
}

showPost(BuildContext context, Post post) {
  Navigator.push(
      context, MaterialPageRoute(builder: (context) => PostViewScreen(post)));
}
